# Nomad Job: Traefik

Nomad job to deploy an instance of [traefik proxy](https://traefik.io/traefik/).

The packer `main.pkr.hcl` file has three builds:
  - `services:traefik:validate`: Build to execute the nomad job validate, which validates the nomad HCL config file. 
  - `services:traefik:plan`: Build to execute the nomad job configurations. 
  - `services:traefik:run`: Build to execute the nomad job configurations.

## Running the build

Execute in an environment with the follosing installed:
  - [packer](https://www.packer.io): to execute the packer build.
  - [nomad](https://www.nomadproject.io): to execute nomad via packer's [local-shell](https://developer.hashicorp.com/packer/docs/provisioners/shell-local) provisioner.
  - [tailscale](https://tailscale.com): to communicate with the nomad cluster.

**Note:** The hashistack/nomad-docker image has all the requried dependenices. It can be used as follows:
```bash
docker run \
   --rm \
   --privileged \
   --device /dev/net/tun:/dev/net/tun \
   -e TAILSCALE_AUTHKEY={docker-exec-tailscale-auth-key} \
   -v ${PWD}:/project_root \
   hashistack/docker-nomad:local {command}
```

Defind the following variables in a file named `variables-override.auto.pkrvars.hcl`:
- `nomad_cluster_http_addres`: This is the full http address (scheme://host:port) of the nomad cluster where the job will be deployed.
- `project_root`: This is the full path to the project/repo root.

### Execute Job Plan (dry-run)
```bash
packer build -only="services:traefik:validate*" packer/nomad-jobs/services/traefik
```

### Execute Job Plan (dry-run)
```bash
packer build -only="services:traefik:plan*" packer/nomad-jobs/services/traefik
```

### Execute Job Run (full-run)
```bash
packer build -only="services:traefik:run*" packer/nomad-jobs/services/traefik
```
