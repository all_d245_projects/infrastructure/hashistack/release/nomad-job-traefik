variable "nomad_cluster_http_address" {
  type        = string
  description = "full http address (scheme://host:port) of the nomad cluster where the job will be deployed."
}
variable "project_root" {
  type        = string
  description = "root to the project"
}
variable "contraint_hostname" {
  type = string
}
variable "consul_catalog_enpoint_address" {
  type = string
}
variable "consul_catalog_enpoint_scheme" {
  type = string
}
variable "traefik_dashboard_host" {
  type = string
}
local "job_root" {
  expression = "${var.project_root}/packer/nomad-jobs/services/traefik"
}