source "null" "no-build" {
  communicator = "none"
}

build {
  name    = "services:traefik:validate"
  sources = ["null.no-build"]

  provisioner "shell-local" {
    inline = [
      "nomad job validate ${local.job_root}/jobs/traefik.nomad.hcl",
    ]
    env = {
      NOMAD_VAR_job_root = "${local.job_root}"
      NOMAD_VAR_contraint_hostname = "${var.contraint_hostname}"
      NOMAD_VAR_consul_catalog_enpoint_address = "${var.consul_catalog_enpoint_address}"
      NOMAD_VAR_consul_catalog_enpoint_scheme = "${var.consul_catalog_enpoint_scheme}"
      NOMAD_VAR_traefik_dashboard_host = "${var.traefik_dashboard_host}"
    }
  }
}
build {
  name    = "services:traefik:plan"
  sources = ["null.no-build"]

  provisioner "shell-local" {
    inline = [
      "nomad job plan ${local.job_root}/jobs/traefik.nomad.hcl",
    ]
    valid_exit_codes = [0,1]
    env = {
      NOMAD_ADDR = var.nomad_cluster_http_address
      NOMAD_VAR_job_root = "${local.job_root}"
      NOMAD_VAR_contraint_hostname = "${var.contraint_hostname}"
      NOMAD_VAR_consul_catalog_enpoint_address = "${var.consul_catalog_enpoint_address}"
      NOMAD_VAR_consul_catalog_enpoint_scheme = "${var.consul_catalog_enpoint_scheme}"
      NOMAD_VAR_traefik_dashboard_host = "${var.traefik_dashboard_host}"
    }
  }
}
build {
  name    = "services:traefik:run"
  sources = ["null.no-build"]

  provisioner "shell-local" {
    inline = [
      "nomad job run -detach ${local.job_root}/jobs/traefik.nomad.hcl"
    ]
    valid_exit_codes = [0,1]
    env = {
      NOMAD_ADDR = var.nomad_cluster_http_address
      NOMAD_VAR_job_root = "${local.job_root}"
      NOMAD_VAR_contraint_hostname = "${var.contraint_hostname}"
      NOMAD_VAR_consul_catalog_enpoint_address = "${var.consul_catalog_enpoint_address}"
      NOMAD_VAR_consul_catalog_enpoint_scheme = "${var.consul_catalog_enpoint_scheme}"
      NOMAD_VAR_traefik_dashboard_host = "${var.traefik_dashboard_host}"
    }
  }
}