variable "job_root" {
  type = string
}
variable "contraint_hostname" {
  type = string
}
variable "consul_catalog_enpoint_address" {
  type = string
}
variable "consul_catalog_enpoint_scheme" {
  type = string
}
variable "consul_catalog_enpoint_tls_ca_file" {
  type    = string
  default = "hamsterwheel_ca.crt"
}
variable "traefik_dashboard_host" {
  type = string
}
variable "traefik_log_level" {
  type    = string
  default = "DEBUG"
}
variable "traefik_provider_file_directory_path" {
  type    = string
  default = "/opt/nomad/volumes"
}

  

job "traefik" {
  region      = "global"
  datacenters = ["dc1"]
  type        = "service"

  group "traefik" {
    update {
      auto_revert = true
    }

    network {
      port "private_web" {
        static       = 80
        host_network = "tailscale"
      }

      port "private_websecure" {
        static       = 443
        host_network = "tailscale"
      }

      port "ping" {
        to           = 8082
        host_network = "tailscale"
      }

      port "api" {
        static       = 8080
        host_network = "tailscale"
      }
    }

    volume "shared-tls" {
      type      = "host"
      source    = "shared-tls"
      read_only = true
    }

    service {
      name = "traefik-web-private-port"
      port = "private_web"
      tags = [ "traefik" ]
 
      check {
        name     = "traefik http port" 
        type     = "tcp"
        interval = "10s"
        timeout  = "5s"
      }
    }

    service {
      name = "traefik-websecure-private-port"
      port = "private_websecure"
      tags = [ "traefik" ]
 
      check {
        name     = "traefik https port" 
        type     = "tcp"
        interval = "10s"
        timeout  = "5s"
      }
    }
 
    service {
      name = "traefik-ping-private-port"
      port = "ping"
      tags = ["traefik"]
 
      check {
        name     = "traefik ping port" 
        type     = "tcp"
        interval = "10s"
        timeout  = "5s"
      }
    }
 
    service {
      name = "traefik-api-private-port"
      port = "ping"
      tags = ["traefik"]
 
      check {
        name = "traefik api ping"
        type     = "http"
        path     = "/ping"
        interval = "10s"
        timeout  = "5s"
      }
    }

    task "traefik" {
      driver = "docker"

      config {
        image = "docker.io/library/traefik:v2.11"
        ports = ["private_web", "private_websecure", "api", "ping"]
        args = [
          "--configFile=${NOMAD_TASK_DIR}/config/traefik.yml"
        ]
      }

      template {
        data = file("${var.job_root}/templates/traefik-static-config.yml")
        destination = "${NOMAD_TASK_DIR}/config/traefik.yml"
      }
      template {
        data = file("${var.job_root}/templates/traefik-http-routers-config.yml")
        destination = "${NOMAD_TASK_DIR}/config/dynamic-configs/traefik-http-routers.yml"
      }
      template {
        data = file("${var.job_root}/templates/traefik-middleware-config.yml")
        destination = "${NOMAD_TASK_DIR}/config/dynamic-configs/traefik-middleware.yml"
      }

      volume_mount {
        volume      = "shared-tls"
        destination = "${NOMAD_ALLOC_DIR}/tls/consul_connect"
      }
      
      env {
        CONSUL_CATALOG_ENDPOINT_ADDRESS     = "${var.consul_catalog_enpoint_address}"
        CONSUL_CATALOG_ENDPOINT_SCHEME      = "${var.consul_catalog_enpoint_scheme}"
        CONSUL_CATALOG_ENDPOINT_TLS_CA_FILE = "${var.consul_catalog_enpoint_tls_ca_file}"
        TRAEFIK_DASHBOARD_HOST              = "${var.traefik_dashboard_host}"
        TRAEFIK_LOG_LEVEL                   = "${var.traefik_log_level}"
      }

      constraint {
        attribute = "${attr.unique.hostname}"
        value     = "${var.contraint_hostname}"
      }
    }
  }
}
